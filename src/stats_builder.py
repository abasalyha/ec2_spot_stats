# -*- coding: UTF-8 -*-

# Created by Andrey Basalyha

import os
from datetime import datetime, timedelta
import boto
import time
import multiprocessing
import cPickle
import numpy
from jinja2 import Template


# Enter your AWS credentials
aws_key = ''
aws_secret = ''


# Settings
aws_ts_mask = "%Y-%m-%dT%H:%M:%S.000Z"
core_max_bid = 0.1
core_min_bid = 0.005
core_bid_step = 0.005
instances_core_map = {
    "cc1.4xlarge": 16,
    "cc2.8xlarge": 32,
    "c1.xlarge": 8,
    "m1.xlarge": 4,
    "m3.xlarge": 4,
#    "m3.xlarge": 4,
}
DATA_MAP = dict.fromkeys(instances_core_map, dict())
core_bid_range = numpy.arange(
    core_min_bid, core_max_bid + core_bid_step, core_bid_step
)


conn = boto.connect_ec2(aws_key, aws_secret)

availability_zones = [zone.name for zone in conn.get_all_zones()]
end_time_stats = datetime.now()
start_time_stats = end_time_stats - timedelta(days=30)

cache_file_format = "{instance_type}_{zone}_price_history.cache"


def get_instance_info_for_bid(instance_type, zone, start_time, end_time, core_bid):
    """

    Get availability from 24h mode for instance in current zone
    using determined core bid

    :param instance_type: instance to be calculated
    :param zone: availability zone in region
    :param start_time: start time point for price history
    :param end_time: end time point for price history
    :param core_bid: bid for core in instance
    :return: available hours for current bid in given zone extrapolated to 24h
    :rtype: float

    """

    bid = core_bid * instances_core_map[instance_type]
    cache_file_name = cache_file_format.format(
        instance_type=instance_type, zone=zone
    )
    if not os.path.exists(cache_file_name):

        prices = conn.get_spot_price_history(
            instance_type=instance_type,
            start_time=start_time.strftime(aws_ts_mask),
            end_time=end_time.strftime(aws_ts_mask),
            availability_zone=zone
        )
        cache_file = open(cache_file_name, "wb")

        # Caching price data
        cPickle.dump(prices, cache_file)
    else:
        # Reading from cache
        cache_file = open(cache_file_name, "rb")
        prices = cPickle.load(cache_file)
        cache_file.close()

    prices.reverse()
    i = 0
    length = len(prices)
    available_seconds = 0.0
    for price in prices:
        price_timestamp = datetime.strptime(price.timestamp, aws_ts_mask)


        if not i == length - 1:
            end_period = datetime.strptime(prices[i+1].timestamp, aws_ts_mask)
            if price.price <= bid:
                available_seconds += (end_period - price_timestamp).seconds

        i+=1
    if prices:
        start_time = datetime.strptime(prices[0].timestamp, aws_ts_mask)
        end_time = datetime.strptime(prices[-1].timestamp, aws_ts_mask)
        total_seconds = (end_time - start_time).total_seconds()

        hours = round(24.0 * (available_seconds / total_seconds), 1)

        print "Getting info for instance {}, core_bid={}, {} hours: {}".format(
            instance_type, core_bid, zone, hours
        )
        return  round(24.0 * (available_seconds / total_seconds), 1), not bool(prices)
    else:
        return 0.0, not bool(prices)


def calculate_instance_stats(i, iq):
    collected_data = {}
    instance_type = iq.get()
    print "Initializing separate worker for instance %s" % instance_type
    availability_hours = dict.fromkeys(core_bid_range, [])
    for zone in availability_zones:
        zone_bids = dict.fromkeys(availability_zones, {})
        for core_bid in core_bid_range:
            hours, empty = get_instance_info_for_bid(
                instance_type=instance_type,
                zone=zone,
                start_time=start_time_stats,
                end_time=end_time_stats,
                core_bid=core_bid
            )
            # Accept only stats where
            if not empty:
                availability_hours[core_bid].append(hours)

            zone_bids[zone][core_bid] = hours

    availability_hours = {}
    for bid in core_bid_range:
        for zone in zone_bids:
                if not bid in availability_hours:
                    availability_hours[bid] = []
                availability_hours[bid].append(zone_bids[zone][bid])


    for core_bid, hours_list in availability_hours.iteritems():
        if hours_list:

            avg_hours = sum(hours_list) / len(hours_list)
        else:
            avg_hours = 0.0

        collected_data[core_bid] = avg_hours

    dump_fn = "{}.pkl".format(instance_type)
    dump_file = open(dump_fn, "w")

    cPickle.dump(collected_data, dump_file)
    print "Writing data in {}".format(dump_fn)
    iq.task_done()

def render_lifetime_chart():
    in_queue = multiprocessing.JoinableQueue()
    t_start_fill = time.time()
    for instance_type in instances_core_map.keys():
        in_queue.put(instance_type)
    t_start_calc = time.time()

    # Starting separate worker for every instance from list
    for i in xrange(len(instances_core_map.keys())):
        worker = multiprocessing.Process(target=calculate_instance_stats, args=(i, in_queue))
        worker.daemon = True
        worker.start()

    # Waiting results
    in_queue.join()


    t_end = time.time()
    print "*"*20
    print t_end - t_start_fill
    print t_end - t_start_calc

    print "Rendering chart"

    chart_matrix = []
    instance_data = dict.fromkeys(instances_core_map.keys(), {})
    print "Loading stats in memory"

    for instance_type in instances_core_map.keys():
        dump_fn = "{}.pkl".format(instance_type)
        dump_file = open(dump_fn, "r")
        print "Open dump file %s"  % dump_fn
        data = cPickle.load(dump_file)

        instance_data[instance_type] = data
        dump_file.close()


    for bid in core_bid_range:
        row = [bid]
        for instance in instances_core_map.keys():
            row.append(instance_data[instance].get(bid, 0.0))
        chart_matrix.append(row)
    template_file = open("chart_template.html", "r")
    template = Template(template_file.read())
    rendered_str = template.render(
        chart_matrix=chart_matrix, instances=instances_core_map.keys()
    )
    result_chart_file = open("result_chart.html", "w")
    result_chart_file.write(rendered_str)
    result_chart_file.close()

    print "Deleting temp files"
    os.system("rm *.cache *.pkl")
    print "Chart rendered: result_chart.html"

if __name__ == "__main__":
    render_lifetime_chart()

Installation
==================

1) Download script using: git clone https://bitbucket.org/abasalyha/ec2_spot_stats.git
2) Go to "src" directory and install requirements: pip install requirements.txt
NOTE: Use virtual environment to install dependencies to prevent overriding existing versions of libraries

Usage
==================
Go to src directory
open "stats_builder.py" and set variables "aws_key", "aws_secret" (keys to access AWS API)
start script using python stats_builder.py
Output chart will be rendered to "result_chart.html"